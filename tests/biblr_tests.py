import os
import sys
sys.path.append('../')
from biblr import biblr, books
import io
import bz2
import csv
import sqlite3
from pathlib import Path
import unittest
import tempfile
import flask

from flask import (
        Flask, request, session, g, redirect, url_for, abort, render_template,
        flash
)

app = flask.Flask(__name__)
class BiblrTestCase(unittest.TestCase):

    def setUp(self):
        self.db_fd, biblr.app.config['DATABASE'] = tempfile.mkstemp()
        biblr.app.testing = True
        self.app = biblr.app.test_client()
        with biblr.app.app_context():
            biblr.init_db()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(biblr.app.config['DATABASE'])

    def login(self, username, password):
        return self.app.post('/login', data=dict(
                username=username,
                password=password
        ), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

    with app.test_request_context('/add'):
#gave a template to plug into comment section rather than manual importing
        def test_add_comment(self):
            with app.test_request_context():
                db = self.get_db()
                db.execute('insert into comments (book, chapter, verse, text)
                            values (?,?,?,?)',
                            ('ge', 1, 1, 'comment test for biblr_tests'))
                db.commit()
                assert b'New entry was successfully posted'

if __name__ == '__main__':
    unittest.main()
